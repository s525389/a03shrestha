var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body

var app = express()  // make express app
var server = require('http').createServer(app)  // inject app into the server

app.use(express.static(__dirname + '/assets'))
app.use(express.static(__dirname + '/views'))

app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine

// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
    response.render("Shrestha_Nista")
  })
  app.get("/Shrestha_Nista", function (request, response) {
    response.render("Shrestha_Nista")
  })
  app.get("/about", function (request, response) {
    response.render("Shrestha_Nista")
  })
  app.get("/new-entry", function (request, response) {
    response.render("new-entry")
  })
  app.get("/Yourchoice", function (request, response) {
    response.render("Yourchoice")
 })
  app.get("/Contacts", function (request, response) {
    response.render("Contacts")
  })
  app.get("/guestbook", function (request, response) {
    response.render("index")
  })
  // 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  //console.log(request.body);  
  if (!request.body.title || !request.body.body) {
      response.status(400).send("Entries must have a title and a body.")
      return;
    }
    entries.push({  // store it
      title: request.body.title,
      content: request.body.body,
      published: new Date()
    })
    response.redirect("/guestbook");  // where to go next? Let's go to the home page :)
  })

  app.post("/Contact", function (request, response) {

  var api_key = 'key-b9dfc529011e85e7c4e156b322acc69e';
  var domain = 'sandbox839f1944cdd6457d803e0d8577cc9293.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'postmaster@sandbox839f1944cdd6457d803e0d8577cc9293.mailgun.org',
    to: 's525389@nwmissouri.edu',
    subject: request.body.email,
    text: request.body.message
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if (!error)
    response.send("Mail sent!");
    else
    response.send("Mail not send!");
  });  

  });
server.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/')
});

