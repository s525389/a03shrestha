
function Acc() {
    var vel = parseFloat(document.getElementById('Velocity').value);
    var time = parseFloat(document.getElementById('Time').value);
    if (vel < 0) {
        alert("Velocity cannot be negative");
    }

    else if (time < 0) {
        alert("Time cannot be negative");
    }
    else if (isNaN(vel)) {
        alert("Velocity should be number.");
    }
    else if (isNaN(time)) {
        alert("Time should be number.");
    }

    else {
        $("#result").html("The acceleration of the vehicle is: " + Accler(vel, time));
        $("#result").css("color","red");
    }
}

function Accler(vel, time) {
    if ( time != 0) {
        return vel / time;
    }
    else{
        throw Error("The given type cannot be zero")
    }

    if (time<0){
        throw Error("The given time cannot be negative")
    }
}
